# SayOK

Aplicação para uso na disciplina Sistemas Distribuídos.

## Descrevendo o processo da atividade

1 - A aplicação Spring foi criada;

2 - Em application.properties foi adicionada a seguinte propriedade:
- `server.port=${WSPORT:8081}`

Com o `${WSPORT:8081}` a aplicação vai buscar o valor no parâmetro "WSPORT". Caso o parâmetro não exista será atribuito o valor 8081 por padrão.

3 - O .jar da aplicação foi criado:

- `mvn clean package`

4 - O Dockerfile foi criado:

```
FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
CMD ["java","-jar","/app.jar"]
```

5 - A aplicação foi enviada para um repositório git;

6 - Dentro do servidor foi clonada a aplicação;

7 - A imagem da aplicação foi construída e o container criado (a porta escolhida foi a 8080):

- `docker build --tag ian/sayok .;docker run -e WSPORT=8080 -p 8080:8080 --name ian_sayok ian/sayok`

### Eh isso :)