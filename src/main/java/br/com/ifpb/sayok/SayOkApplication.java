package br.com.ifpb.sayok;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SayOkApplication {
    public static void main(String[] args) {
        SpringApplication.run(SayOkApplication.class, args);
    }

}
