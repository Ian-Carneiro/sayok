package br.com.ifpb.sayok.controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/status")
public class SayOkController {
    @GetMapping()
    private String sayOk(){
        return "Ok";
    }
}
